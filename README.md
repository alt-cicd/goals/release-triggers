<img src="docs/images/alt-logo.png" alt="alt-cicd" width="300"/>

<a name="heading">alt-cicd module: goals/release-triggers</a>
==============================================


<a name="titlebar">Introduction</a>
---------------------------

The **alt-cicd/goals/release-triggers**  module provides the child pipelines required by other
alt-cicd library modules to create pre-release and final-release artifacts.

## <a name="contents">Contents</a>
1. [Variables](#variables)
4. [Templates](#templates)
   1. [Variables](#template-variables)
   2. [Rules](#template-rules)
   3. [Scripts](#template-scripts)
   1. [Jobs](#template-jobs)
5. [Jobs](#jobs)
   1. [trigger_prerelease_with_build](#trigger_prerelease_with_build)
   1. [trigger_prerelease_with_author](#trigger_prerelease_with_author)
   1. [trigger_prerelease_with_user_id](#trigger_prerelease_with_user_id)
   1. [trigger_prerelease_with_user_login](#trigger_prerelease_with_user_login)
   1. [trigger_prerelease_with_changeset](#trigger_prerelease_with_changeset)
   1. [trigger_prerelease_with_protected_ref](#trigger_prerelease_with_protected_ref)
   1. [trigger_prerelease_with_commit_ref](#trigger_prerelease_with_commit_ref)
   1. [trigger_prerelease_with_commit_sha](#trigger_prerelease_with_commit_sha)
   1. [trigger_prerelease_with_sha256sum](#trigger_prerelease_with_sha256sum)
   1. [trigger_final_release_on_commit_ref](#trigger_final_release_on_commit_ref)
   1. [trigger_final_release_as_latest_on_commit_ref](#trigger_final_release_as_latest_on_commit_ref)


<a name="variables">Variables</a>
========================================

All CICD variables are documented in-line.  The current set of release goal variables is:

```yaml
variables:
  CICD_GOAL_PRE_RELEASE_ENABLED: 'false'                                             # Pre-release goal child pipeline trigger will not run if not set to true
  CICD_GOAL_PRE_RELEASE_EXCLUDE_ON_COMMIT_REF: 'false'                               # Exclude pre-releases on branch pattern when enabled
  CICD_GOAL_PRE_RELEASE_EXCLUDE_ON_COMMIT_REF_PATTERN: '/^(main|master)$/'           # Pre-release goal child pipeline will NOT CI_COMMIT_REF_NAME matches pattern
  CICD_GOAL_PRE_RELEASE_COMMIT_REF_PATTERN: '/^(main|master)$/'                      # Pre-release goal child pipeline will trigger if enabled and CI_COMMIT_REF_NAME matches pattern
  CICD_GOAL_PRE_RELEASE_AUTHOR_PATTERN: '/^(gitusername|someothername)$/'            # Deployment goal child pipeline will trigger if enabled and CICD_COMMIT_AUTHOR matches pattern
  CICD_GOAL_PRE_RELEASE_USER_PATTERN: '/^(userid|username)$/'                        # Deployment goal child pipeline will trigger if enabled and GITLAB_USER_ID or GITLAB_USER_LOGIN matches pattern
  CICD_GOAL_PRE_RELEASE_PROTECTED_REF_ENABLED: 'false'                               # Deployment goal child pipeline will trigger if enabled and CI_COMMIT_REF_PROTECTED is true
  CICD_GOAL_FINAL_RELEASE_ENABLED: 'false'                                           # Final release goal child pipeline trigger will not run if not set to true
  CICD_GOAL_FINAL_RELEASE_AS_LATEST_ENABLED: 'false'                                 # "latest" release goal child pipeline trigger will not run if not set to true
  CICD_GOAL_FINAL_RELEASE_COMMIT_REF_PATTERN: '/^(main|master)$/'                    # Final release goal child pipeline will trigger if enabled and CI_COMMIT_REF_NAME matches pattern
  CICD_MODULE_GOALS_RELEASE_TRIGGERS: 'true'                                         # Identifies that the goals-release-triggers module is included / active.
  CICD_MODULE_GOALS_RELEASE_TRIGGERS_VERSION: '1.0.0'                                # Identifies the goals-release-triggers  module version
  CICD_VERSION_LATEST: 'latest'                                                      # Version used to identify as latest (or stable, current etc)
```

<a name="templates">Templates</a>
======================================

## <a name="template-variables">Variables</a>

The module contains a matching variables template, including the name of the module, which jobs in the module
extend:

```yaml
#----------------------------------------------------------
# Templates - Variables
#----------------------------------------------------------

.goals_release_triggers:                                                                            # Module specific variable over-rides, referenced in module jobs
   variables:
      CICD_MODULE_NAME: 'goals-release-triggers'
```
## <a name="template-rules">Rules</a>

None defined.

##<a name="template-scripts">Script</a>

None defined.

## <a name="template-jobs">Jobs</a>

None defined.

<a name="jobs">Jobs</a>
======================================

The goals-release module defines the following trigger jobs, to create releases with various metadata (or none/final/latest).

* `trigger_prerelease_with_build:`
* `trigger_prerelease_with_author:`
* `trigger_prerelease_with_user_id:`
* `trigger_prerelease_with_user_login:`
* `trigger_prerelease_with_changeset:`
* `trigger_prerelease_with_protected_ref:`
* `trigger_prerelease_with_commit_ref:`
* `trigger_prerelease_with_commit_sha:`
* `trigger_prerelease_with_sha256sum:`
* `trigger_final_release_on_commit_ref:`
* `trigger_final_release_as_latest_on_commit_ref:`
